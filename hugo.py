#!/usr/bin/env python3
import gi
from gi.repository import Ide, Gio, GObject, GLib

gi.require_version('Ide', '1.0')

class HugoBuildSystem(Ide.Object, Ide.BuildSystem, Gio.AsyncInitable):
    project_file = GObject.Property(type=Gio.File)

    def do_get_id(self):
        return 'hugo'

    def do_get_display_name(self):
        return 'Hugo'

    def do_get_priority(self):
        0

    def do_init_async(self, priority, cancel, callback, data=None):
        task = Gio.Task.new(self, cancel, callback)
        task.set_priority(priority)
        task.return_boolean(True)

    def do_init_finish(self, task):
        return task.propagate_boolean()

class HugoBuildPipelineAddin(Ide.Object, Ide.BuildPipelineAddin):
    def do_load(self, pipeline):
        context = pipeline.get_context()
        build_system = context.get_build_system()

        if type(build_system) != HugoBuildSystem:
            return

        srcdir = pipeline.get_srcdir()

        config = pipeline.get_configuration()
        runtime = config.get_runtime()

        if not runtime.contains_program_in_path('hugo'):
            raise OSError('The runtime must contain `hugo`')

        try:
            launcher = pipeline.create_launcher()
            launcher.set_cwd(srcdir)
            launcher.push_argv('hugo')
            launcher.push_argv('--verbose')
        except AttributeError as ex:
            print("Failed to create launcher: " + repr(ex))
            return

        build_stage = Ide.BuildStageLauncher.new(context, launcher)
        self.track(pipeline.connect(Ide.BuildPhase.BUILD, 0, build_stage))

    def do_unload(self, application):
        pass

    def _query(self, stage, pipeline, cancellable):
        stage.set_completed(False)
