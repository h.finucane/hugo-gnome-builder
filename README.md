# a simple hugo plugin for gnome builder

To install
```
# git clone git@gitlab.gnome.org:h.finucane/hugo-gnome-builder.git ~/.local/share/gnome-builder/plugins/hugo
```

It makes the `build` button build your [hugo](https://gohugo.io/) project.
